Môi trường cài đặt:
```
[root@node1 ~]# cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

192.168.10.70	node1
192.168.10.71	node2
```
# Disable selinux

### In /etc/sysconfig/selinux , change the following lines:
```
sed -i 's/\(^SELINUX=\).*/\SELINUX=disabled/' /etc/sysconfig/selinux
sed -i 's/\(^SELINUX=\).*/\SELINUX=disabled/' /etc/selinux/config
```
# Disable firewall
```
systemctl disable firewalld
reboot
```
# Update Your System
```
yum -y update
yum -y groupinstall core base "Development Tools"
```
# Install Additional Required Dependencies
```
yum -y install lynx mariadb-server mariadb php php-mysql php-mbstring tftp-server \
  httpd ncurses-devel sendmail sendmail-cf sox newt-devel libxml2-devel libtiff-devel \
  audiofile-devel gtk2-devel subversion kernel-devel git php-process crontabs cronie \
  cronie-anacron wget vim php-xml uuid-devel sqlite-devel net-tools gnutls-devel php-pear unixODBC mysql-connector-odbc
```
# Install Legacy Pear requirements
```
pear install Console_Getopt
```
# Enable and Start MariaDB
You must have MariaDB running for freepbx to operate normally.  It must start automatically, and be running continuously.
```
systemctl enable mariadb.service
systemctl start mariadb
```
Now that our MariaDB database is running, we want to run a simple security script that will remove some dangerous defaults and lock down access to our database system a little bit
```
mysql_secure_installation
```
The prompt will ask you for your current root password. Since you just installed MySQL, you most likely won’t have one, so leave it blank by pressing enter. Then the prompt will ask you if you want to set a root password. Do not set a root password. We secure the database automatically, as part of the install script.  Apart from that you can chose yes for the rest. This will remove some sample users and databases, disable remote root logins, and load these new rules so that MySQL immediately respects the changes we have made.

# Enable and Start Apache
FreePBX uses the Apache web server, so this must be started and running.
```
systemctl enable httpd.service
systemctl start httpd.service
```
# Add the Asterisk User
```
adduser asterisk -m -c "Asterisk User"
```
# Install and Configure Asterisk
Download Asterisk source files.
```
cd /usr/src
wget https://downloads.asterisk.org/pub/telephony/asterisk/old-releases/asterisk-13.0.0.tar.gz
wget wget -O jansson.tar.gz https://github.com/akheron/jansson/archive/v2.7.tar.gz
wget https://github.com/pjsip/pjproject/archive/refs/tags/2.4.tar.gz

```
# List source file:
```
ll
total 38780
-rw-r--r-- 1 root root  7605671 Jun 29 15:30 2.4.tar.gz
-rw-r--r-- 1 root root 31983163 Oct 24  2014 asterisk-13.0.0.tar.gz
-rw-r--r-- 1 root root   114743 Jun 29 15:30 jansson.tar.gz
```
# Compile and install pjproject
```
cd /usr/src
tar vxfz 2.4.tar.gz
cd pjproject-2.4
CFLAGS='-DPJ_HAS_IPV6=1' ./configure --prefix=/usr --enable-shared --disable-sound\
  --disable-resample --disable-video --disable-opencore-amr --libdir=/usr/lib64
make dep
make
make install
```
# Compile and Install jansson
```
cd /usr/src
tar vxfz jansson.tar.gz
cd jansson-*
autoreconf -i
./configure --libdir=/usr/lib64
make
make install
```
# Compile and install Asterisk
```

cd /usr/src
tar xvfz asterisk-13.0.0.tar.gz
rm -f asterisk-13.0.0.tar.gz
cd asterisk-*
contrib/scripts/install_prereq install
./configure --libdir=/usr/lib64
contrib/scripts/get_mp3_source.sh
make menuselect
```
You will be prompted at the point to pick which modules to build. Most of them will already be enabled, but if you want to have MP3 support (eg, for Music on Hold), you need to manually turn on 'format_mp3' on the first page.

# After selecting 'Save & Exit' you can then continue
```
make
make install
make config
ldconfig
chkconfig asterisk off
```
# Set Asterisk ownership permissions.
```
chown asterisk. /var/run/asterisk
chown -R asterisk. /etc/asterisk
chown -R asterisk. /var/{lib,log,spool}/asterisk
chown -R asterisk. /usr/lib64/asterisk
chown -R asterisk. /var/www/
```
# Install and Configure FreePBX
A few small modifications to Apache.
```
sed -i 's/\(^upload_max_filesize = \).*/\120M/' /etc/php.ini
sed -i 's/^\(User\|Group\).*/\1 asterisk/' /etc/httpd/conf/httpd.conf
sed -i 's/AllowOverride None/AllowOverride All/' /etc/httpd/conf/httpd.conf
systemctl restart httpd.service
```
# Download and install FreePBX.
```
cd /usr/src
wget http://mirror.freepbx.org/modules/packages/freepbx/freepbx-13.0-latest.tgz
tar xfz freepbx-13.0-latest.tgz
rm -f freepbx-13.0-latest.tgz
cd freepbx
./start_asterisk start
./install -n
```
# Create systemd startup script for FreePBX
```
nano /etc/systemd/system/freepbx.service
[Unit]
Description=FreePBX VoIP Server
After=mariadb.service
 
[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/usr/sbin/fwconsole start -q
ExecStop=/usr/sbin/fwconsole stop -q
 
[Install]
WantedBy=multi-user.target
```
# From there you can enable it so it starts automatically
```
systemctl enable freepbx
systemctl start freepbx
```
# Done. We can login at: http://192.168.10.70

# Source: https://wiki.freepbx.org/display/FOP/Installing+FreePBX+13+on+CentOS+7
